
This boilerplate project serve as demo to learn and master asynchronous Javascript code which features callbacks, Async Await calls and promises.

Asynchronous code enables us to do something else while doing a time consuming-task like fetching some hundreds of users at once.